import request from '../libs/request'
/**
 * 用户登录
 * @param username
 * @param password
 */
export const login = ({loginName, passWord}) => {
  const data = {
    loginName: loginName,
    passWord: passWord
  }
  return request({
    url: 'user/login',
    data,
    method: 'post'
  })
}

// 登出
export const logout = () => {
  return request ({
    url: 'user/logout',
    method: 'get'
  })
}

// 获取用户信息
export const getUserInfo = (loginName) => {
  const params = {loginName: loginName}
  return request({
    url: 'user/getUserByCondition',
    params: params,
    method: 'get'
  })
}

export const addNewUser = (data) => {
  return request({
    url: 'user/add',
    data,
    method: 'post'
  })
}

export const tabSearch = (onSearchContent) => {
  const params = {params: onSearchContent }
  return request({
    url: 'video/tabSearch',
    params,
    method: 'get'
  })
}

export const updateUser = (data) => {
  return request({
    url: 'user/update',
    data,
    method: 'post'
  })
}
export const getUserCountInfo = (params) => {
  return request({
    url: 'video/getUserLikesCount',
    params,
    method: 'get'
  })
}
