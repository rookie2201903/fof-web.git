import request from '../libs/request'

const uploadFile = (data)=>{
  return request({
    url: 'fof/user/upload',
    data,
    method: 'post'
  })
}

export default {
  uploadFile
}
