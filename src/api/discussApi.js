import request from '../libs/request'

export const getDiscussByCondition = (params) => {
  return request({
    url: 'discuss/getDiscussByCondition',
    params,
    method: 'get'
  })
}

export const addDiscuss = (data) => {
  return request({
    url: 'discuss/addDiscuss',
    data,
    method: 'post'
  })
}

export const updateDiscuss = (params) => {
  return request({
    url: 'discuss/updateDiscuss',
    params,
    method: 'get'
  })
}
export const getUserOperationData = (params) => {
  return request({
    url: 'userOperation/getUserOperation',
    params,
    method: 'get'
  })
}



