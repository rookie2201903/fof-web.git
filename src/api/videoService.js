import request from '../libs/request'
/**
 * 获取当前用户上传的视频
 * @param
 * @param
 */
export const getVideoList = (params) => {
  return request({
    url: 'video/videoList',
    params,
    method: 'get'
  })
}
export const updateVideoInfo = (params) => {
  return request({
    url: 'video/updateVideoInfo',
    params,
    method: 'get'
  })
}

export const getOneVideoInfo = (params) => {
  return request({
    url: 'video/getOneVideoInfo',
    params,
    method: 'get'
  })
}

export const getLikesSix = () => {
  return request({
    url: 'video/getLikesSix',
    method: 'get'
  })
}
export const  getCommentSix = () => {
  return request({
    url: 'video/getCommentSix',
    method: 'get'
  })
}

export const getMySelfVideo = (params) => {
  return request({
    url: 'video/searchMySelfVideo',
    params,
    method: 'get'
  })
}
