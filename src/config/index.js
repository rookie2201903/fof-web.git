module.exports = {
  apiUrl: {
  // dev: 'http://192.168.0.122:'
  },
  /**
   * token在Cookie中存储的天数，默认1天
   */
  cookieExpires: 1,
  /**
   * 项目部署基础
   * 默认情况下，我们假设你的应用将被部署在域的根目录下,
   * 例如：https://www.my-app.com/
   * 默认：'/'
   * 如果您的应用程序部署在子路径中，则需要在这指定子路径
   * 例如：https://www.foobar.com/my-app/
   * 需要将它改为'/my-app/'
   */
  publicPath: {
    //  本地环境发布目录
    dev: '/'
    //  生产环境发布目录
    // pro: '/'
  },
  /**
   * 默认打开的首页的路由name值，默认为home
   */
  homeName: 'firstPage'
}
