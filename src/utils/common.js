import {getCurrentUserInfo} from '../libs/util'

export const getCurrentUserCommon = () => {
  const  user =  eval('(' + getCurrentUserInfo() + ')')
  // const user = JSON.parse(getCurrentUserInfo())
  return user
  // this.headImg = require("../../../assets/image" + file )
}

export const getCurrentUserHeadImg = (userParam) => {
  // debugger
  let fileName = ""
  if (userParam) {
    fileName = getHeadImg(userParam)
    return fileName
  }
  const user = getCurrentUserCommon()
  if (user) {
    fileName = getHeadImg(user)
    console.log("getCurrentUserCommon=>",fileName)
    return fileName
  }
  return null
}

const getHeadImg = (user) => {
  let fileName = ""
  const url = user.headUrl
  if (url.charAt(0) === '.') {
    fileName = url.slice(user.headUrl.lastIndexOf("/") )
    return fileName
  }
  fileName = "/"+url.slice(user.headUrl.lastIndexOf("\\") + 1)
  return fileName
}

export const sortArrayDesc = (array,key1,key2) => {
  return array.sort(function (a,b) {
    // debugger
    let x = new Date(a[key1][key2])
    let y = new Date(b[key1][key2])
    return y-x;
  } )
}
