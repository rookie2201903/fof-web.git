import {getToken, removeToken, setToken, setUserInfo, TOKEN_KEY} from '../../libs/util'
import {login} from '../../api/user'

export default {
  state: {
    phone: '',
    registerDate: '',
    uuid: '',
    fansNum: 0,
    attentionNum: 0,
    loginName: '',
    userId: '',
    token: '',
    headUrl: '',
    eMail: ''
  },
  mutations: {
    setLoginName (state,loginName) {
      state.loginName = loginName
    },
    setUserId (state,userId) {
      state.userId = userId
    },
    setToken (state, token) {
      state.token = token
      setToken(token)
    },
    setHeadUrl (state,headUrl) {
      state.headUrl = headUrl
    },
    setPhone (state,phone) {
      state.phone = phone
    },
    setUuid (state,uuid) {
      state.uuid = uuid
    },
    setRegisterDate (state,registerDate){
      state.registerDate = registerDate
    },
    setFansNum (state,fansNum) {
      state.fansNum = fansNum
    },
    setAttentionNum (state,attentionNum) {
      state.attentionNum = attentionNum
    }

  },
  actions: {
  //登录
    handleLogin ({commit}, {loginName , passWord ,auto}) {
      loginName = loginName.trim()

      return new Promise((resolve,reject) => {
        // debugger
        login({
          loginName,passWord
        }).then(res => {
          console.log("handleLogin--login")

          console.log("res*******",res)
          if (res) {
            if (res.code === 200) {
              const token = res.data.token
              console.log("login===token",token)
              commit('setToken', {token,auto})
              console.log("Cookies.get(TOKEN_KEY)=>",getToken())
              resolve(res)
              console.log("--------------")
            }
          }
        }).catch(err => {
          reject(err)
        })
      })
    },
    //存放用户信息
    getUserInfo ({state,commit},{res}) {
      debugger
      setUserInfo(res.data.loggingUser)
      return new Promise((resolve,reject) => {
        commit('setLoginName',res.data.loggingUser.loginName)
        commit('setHeadUrl',res.data.loggingUser.headUrl)
        if (res.data.token) {
          commit('setToken',res.data.token)
        }
        commit('setUserId',res.data.loggingUser.id)
        commit('setAttentionNum',res.data.loggingUser.attentionNum)
        commit('setFansNum',res.data.loggingUser.fansNum)
        commit('setPhone',res.data.loggingUser.phone)
        commit('setRegisterDate',res.data.loggingUser.registerDate)
        commit('setUuid',res.data.loggingUser.uuid)
      })
    },
    logOut({ commit }) {
      return new Promise(resolve => {
        commit('setToken', '')
        removeToken()
        resolve()
      })
    },

    },

}
