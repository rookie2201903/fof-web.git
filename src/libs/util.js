import Cookies from 'js-cookie'
import  config from '../config'

export const TOKEN_KEY = 'token'
export const CURRENT_USER = 'currentUserInfo'
export const  setToken = (token) => {
  // debugger
  if (true) {
    //expires: 有效期 ，当前默认一天
    Cookies.set(TOKEN_KEY,token, {expires: config.cookieExpires || 1})
  } else {
    Cookies.set(TOKEN_KEY, token)
  }
}
export const setUserInfo = (user) => {
  Cookies.set(CURRENT_USER,user, {expires: 1})
}
export const getCurrentUserInfo = () => {
  const user = Cookies.get(CURRENT_USER)
  return user
}
export const getToken = () => {
  const token = Cookies.get(TOKEN_KEY)
  console.log("getTokenUtil=>",token)
  if (token && token !=='undefined') {
    return token
  } else {
    return false
  }
}

export const removeToken = () => {
  Cookies.remove(TOKEN_KEY)
   const token = getToken()
  console.log("removed**token",token)
}

