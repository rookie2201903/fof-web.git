import axios from 'axios'
// 引入qs模块，用来序列化post类型的数据
import qs from 'qs';
//应该是为了使用iview的message组件来做消息提示
import {Message} from 'view-design'
import {getToken, removeToken} from './util'
import router from '../router'

let baseUrl = 'http://localhost:2424/fof/'
// let baseUrl = process.env.BASE_API
// 多环境配置 环境的切换
// switch (process.env.NODE_ENV) {
//   case 'development':
//     // 这里是本地的请求url
//     baseUrl = 'http://192.16.0.122:2424'
//     break
//   case 'production':
//     // 生产环境url
//     // baseUrl = $config.apiUrl.pro
//     break
// }
/**
 * 创建axios实例
 * @type {AxiosInstance}
 */
const service = axios.create({
  baseURL: baseUrl,
  timeout: 30000
})
// request interceptor(请求拦截器)
service.interceptors.request.use(config => {
  // const token = Vue.ls.get(ACCESS_TOKEN)
  const token = getToken()
  if (token) {
    config.headers['Authorization'] = token // 让每个请求携带自定义 token 请根据实际情况自行修改
  }
  config.headers['Content-Type'] = 'application/json;charset=UTF-8'
  config.method === 'get'
    ? config.params = {...config.params} : config.data = JSON.stringify({...config.data})
  return config
},error => {
  return  Promise.reject(error)
})
/**
 * 响应结果处理
 */
// response interceptor（接收拦截器）
service.interceptors.response.use(
  (response) => {
    const res = response.data
    if (res.code === 0 || res.code === 200) {
      // 服务端定义的响应code码为0或者200时请求成功
      // 使用Promise.resolve 正常响应
      console.log(Promise.resolve(res.data))
      return Promise.resolve(res)
    } else {
      // debugger
      // 使用Promise.reject 响应
      // Message.error({content: res.message})
      return Promise.reject(res.data)
    }
  },  error => { //这里应该是把错误处理放进来了，没有单独写一个错误处理函数
    let message = ''
// debugger
    if (error && error.response) {
      console.log("error=>",error)
      switch (error.response.status) {
        case 401:
          message = '登录过期，请重新登录!'
          removeToken()
          //?
          router.push("login")
          const token = getToken()
          console.log("*********token ",token)
          break
        case 403:
          message = error.response.data.path + ',' + error.response.data.msg
          break
        case 429:
          message = '访问太过频繁，请稍后再试!'
          break
        default:
          message = error.response.data.msg ? error.response.data.msg : '服务器错误'
          break
      }
      Message.error({content: message})
      // 请求错误处理
      return Promise.reject(error)
    } else {
      message = '连接服务器失败'
      Message.error({content: message})
      return Promise.reject(error)
    }
  }
)

export default service
