// 这个main.js文件是程序的唯一入口
// import Vue from "vue/dist/vue.common.js"
import Vue from 'vue'
import App from './App'
// 要是router中的文件名为index，那么路径只写到文件夹就可以，它会自动扫描小面的index.js文件
import router from './router'

import config from './config'
import store from './store'
import iView from 'view-design'
// 引入video-player
import VideoPlayer from 'vue-video-player'
import 'vue-video-player/src/custom-theme.css'
import 'video.js/dist/video-js.css'

// 使用iview需要导入样式
import 'view-design/dist/styles/iview.css'
// 1导入
Vue.config.productionTip = false

// 2显示声明
Vue.use(iView, VideoPlayer)
/**
 * @description 全局注册应用配置(把config配置注入到原型中，用来给多个页面调用)
 */
Vue.prototype.$config = config
Date.prototype.format = function(fmt){
  var o = {
    "M+" : this.getMonth()+1,                 //月份
    "d+" : this.getDate(),                    //日
    "h+" : this.getHours(),                   //小时
    "m+" : this.getMinutes(),                 //分
    "s+" : this.getSeconds(),                 //秒
    "q+" : Math.floor((this.getMonth()+3)/3), //季度
    "S"  : this.getMilliseconds()             //毫秒
  };

  if(/(y+)/.test(fmt)){
    fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));
  }

  for(var k in o){
    if(new RegExp("("+ k +")").test(fmt)){
      fmt = fmt.replace(
        RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
    }
  }

  return fmt;
}
// 全局唯一的vue对象
// eslint-disable-next-line no-new
new Vue({
  el: '#app',
  // 给这个vue对象配置路由
  router,
  // 将store注入到vue的实例中
  store,
  // components: { App },
  // template: '<App/>'
  // iview官网引入iview后使用的推荐的写法
  render: h => h(App)

})
