import Vue from 'vue'
//导入路由
import VueRouter from 'vue-router'
import routers from './routers'
import {getToken} from '../libs/util'
import iView from 'view-design'

const {homeName} = require('../config')
//安装路由
Vue.use(VueRouter)
//要把这个路由配置给别的地方用，就要导出
//相当于 new 一个Router对象 然后export default 来导出，再在对象内部进行配置new Router({})
const router = new VueRouter({
//  把路由的配置选项单独写到一个文件中了
routes: routers
})
const LOGIN_PAGE_NAME = 'login'

const  permitList = [ LOGIN_PAGE_NAME]
//使用 router.beforeEach 注册一个全局前置守卫
router.beforeEach((to,from ,next) => {

  iView.LoadingBar.start()

  const token = getToken()
  console.log("****",!token)
    if (token) {
      next()
    } else {
      if (to.name === LOGIN_PAGE_NAME) {
        next()
      } else {
        // {name: LOGIN_PAGE_NAME}
        next()
      }
}
//   if (token) {
//     next()
//   }
//   else if (token&& permitList.includes(to.name)) {
//     // 已登录且要跳转的页面是登录页
//
//     next( {
//       name: homeName // 跳转到homeName页
//     })
//   }
//   if (!token) {
//     // 未登录,并且不是白名单
//     next({
//       name: LOGIN_PAGE_NAME// 跳转到登录页
//     })
//   }
//   if (!token && permitList.includes(to.name)) {
//     next()//跳转
//   }

})

router.afterEach(to => {
  // debugger
  iView.LoadingBar.finish()
  //scrollTo() 方法可把内容滚动到指定的坐标。
  window.scrollTo(0,0)
})
export default router
//
