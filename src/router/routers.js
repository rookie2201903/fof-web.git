//导入组件
import login from '../components/view/login/login'
import homepage from '../components/view/home/homepage'
import tab from '../components/view/tab/tab'
import mykitchens from '../components/view/mykitchens/mykitchens'
import err from '../components/error/401error'
import videocontent from '../components/view/videocontent/videocontent'
import register from '../components/view/register/register'
import firstPage from '../components/view/ firstpage/firstPage'
import comment from '../components/view/comment/comment'
import searchView from '../components/view/searchView/searchView'
import personalCenter from '../components/view/personalCenter/personalCenter'
import myInfo from '../components/view/personalCenter/myInfo/myInfo'
import myAvatar from '../components/view/personalCenter/myAvatar/myAvatar'
import userInfo from '../components/view/personalCenter/myInfo/userInfo'
import individualKitchen from '../components/view/individualKitchen/individualKitchen'
//具体的路由配置

export default [
  //设置默认路由
  {
    path: '/',
    name: 'firstPage',
    redirect: '/firstPage',
    component: firstPage
  },
  {
    //配置路由路径(与实际路径无关)
    path: '/login',
    name: 'login',
    //要跳去的组件
    component: login
  },
  {
    path: '/homepage',
    name: 'homepage',
    component: homepage,
    redirect: {name: "mykitchens"},
    children:[
      // {
      //   path: '/tab',
      //   name: 'tab',
      //   component: tab
      // },
      {
        path: '/mykitchens',
        name: 'mykitchens',
        component: mykitchens
      },
      {
        path: '/individualKitchen',
        name: 'individualKitchen',
        component: individualKitchen
      }
    ]
  },
{
  path: '/401error',
  name: '401',
  component: err
},
  {
    path: '/videocontent',
    name: 'videocontent',
    component: videocontent
  },
  {
    path: '/register',
    name: 'register',
    component: register
  },
  {
    path: '/firstPage',
    name: 'firstPage',
    component: firstPage,
    // redirect: {name: 'mykitchens'},
    // children: [
    //   {path: '/mykitchens',
    //   name: 'mykitchens',
    //   component: mykitchens}
    // ]
  },
  {
    path: '/comment',
    name: 'comment',
    component: comment
  },
  {
    path: '/searchView',
    name: 'searchView',
    component: searchView
  },
  {
    path: '/personalCenter',
    name: 'personalCenter',
    component: personalCenter,
    redirect: {name: "userInfo"},
    children:
    [
      {
        path: "/userInfo",
        name: "userInfo",
        component: userInfo
      },
      {
        path: "/myAvatar",
        name: 'myAvatar',
        component: myAvatar
      },
      {
        path: "/myInfo",
        name: 'myInfo',
        component: myInfo
      }
    ]
  },

]

